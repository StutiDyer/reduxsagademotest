import { combineReducers } from "redux";
import BasicReducer from "../Redux/Reducers/ReduxReducer";
import ReduxSauceReducer from "../ReduxSauce/index";

export default combineReducers({
  BasicReducer: BasicReducer,
  ReduxSauceReducer: ReduxSauceReducer
});


